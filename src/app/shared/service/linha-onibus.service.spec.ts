import { TestBed } from '@angular/core/testing';

import { LinhaOnibusService } from './linha-onibus.service';

describe('LinhaOnibusService', () => {
  let service: LinhaOnibusService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LinhaOnibusService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
