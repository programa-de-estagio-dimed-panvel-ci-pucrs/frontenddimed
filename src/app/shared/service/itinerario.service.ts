import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, mergeMap, tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { Itinerario } from '../models/itinerario.model';

@Injectable({
  providedIn: 'root'
})
export class ItinerarioService {
  itinerario!: Itinerario;
  constructor(public http: HttpClient) { }

  getItinerario(id: string): Observable<Itinerario>{
    return this.http.get<Itinerario>(environment.URLBASE + '?a=il&p=' + id).pipe(
      catchError(this.errorHandler<Itinerario>(this.itinerario, `Erro ao buscar a tarefa ${id}`))
    );
  }

  public getLatLng(id: string): Promise<number> {
    return this.http.get(environment.URLBASE + '?a=il&p=' + id)
        .toPromise()
        .then((response: any) => {
            return response;
        })  
    }

  errorHandler<T>(resultado?: T, mensagem:string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if(erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    }
  }


}
