import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { Observable, throwError, of } from 'rxjs';
import { LinhaOnibus } from '../models/linhaOnibus.model';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LinhaOnibusService {
  constructor(public http: HttpClient) {
    
   }

   getLinhaOnibus(): Observable<LinhaOnibus[]>{
    return this.http.get<LinhaOnibus[]>(environment.URLBASE + '?a=nc&p=%&t=o').pipe(
      catchError(this.errorHandler<LinhaOnibus[]>([], 'Erro ao buscar linha de ônibus.'))
    );
  }

  getLinhaOnibusPorNome(nome: string): Observable<LinhaOnibus[]>{
    return this.http.get<LinhaOnibus[]>(environment.URLBASE + '?a=nc&p=%&t=todos?nome='+ nome).pipe(
      catchError(this.errorHandler<LinhaOnibus[]>([], 'Erro ao buscar as tarefas'))
    );
  }


  errorHandler<T>(resultado?: T, mensagem:string = 'Erro'){
    return (erro: HttpErrorResponse): Observable<T> => {
      if(erro.error instanceof ErrorEvent){
        console.log(`${mensagem} ${erro.error.message}`);
        return throwError(`${mensagem}: ${erro.error.message}`);
      }else{
        console.log(`${mensagem} - Status: ${erro.status}`);
        return of(resultado as T);
      }
    }
  }


}
