import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { ItinerarioComponent } from './itinerario/itinerario.component';
import { LinhaComponent } from './linha/linha.component';
import { LotacaoComponent } from './lotacao/lotacao.component';

const routes: Routes = [
  { path: '', component: HomeComponent, data: {title: 'Home'} },
  { path: 'linha', component: LinhaComponent, data: {title: 'Lista'} },
  { path: 'lotacao', component: LotacaoComponent, data: {title: 'Lotação' }},
  { path: 'itinerario', component: ItinerarioComponent, data: {title: 'Itinerário' }},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
