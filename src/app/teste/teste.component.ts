import { Component, OnInit } from '@angular/core';
import { LinhaOnibus } from '../shared/models/linhaOnibus.model';
import { LinhaOnibusService } from '../shared/service/linha-onibus.service';

@Component({
  selector: 'app-teste',
  templateUrl: './teste.component.html',
  styleUrls: ['./teste.component.css']
})
export class TesteComponent implements OnInit {
  linhasDeOnibus: LinhaOnibus[] = [];

  constructor(private linhaService: LinhaOnibusService) { }

  ngOnInit(): void {
    this.linhaService.getLinhaOnibus().subscribe((linhasDeOnibus: LinhaOnibus[]) => {
      console.log(linhasDeOnibus);
      this.linhasDeOnibus = linhasDeOnibus;
    } );

    
     console.log(this.linhasDeOnibus);

    
  }

}
