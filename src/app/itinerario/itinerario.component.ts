import { Component, OnInit } from '@angular/core';
import { Itinerario } from '../shared/models/itinerario.model';
import { LatLon } from '../shared/models/latLon.model';
import { ItinerarioService } from '../shared/service/itinerario.service';

@Component({
  selector: 'app-itinerario',
  templateUrl: './itinerario.component.html',
  styleUrls: ['./itinerario.component.css']
})
export class ItinerarioComponent implements OnInit {

   itinerario = {} as Itinerario;
   latLon: Array<LatLon> = [];
   latLonM = {} as number;
  constructor(private itinerarioService: ItinerarioService) { }

  ngOnInit(): void {
    this.itinerarioService.getItinerario('5527').subscribe((itinerario: Itinerario) => {
      console.log(itinerario);
      this.itinerario = itinerario;
      this.itinerario.idlinha = itinerario.idlinha;
      this.itinerario.codigo = itinerario.codigo;
      this.itinerario.nome = itinerario.nome;
    } );

    this.itinerarioService.getLatLng('5527')
    .then((response: any) => {
      for(let i = 0; i < Object.keys(response).length - 3; i++){
        this.latLon.push(response[i]);
      }
      this.itinerario.latLon = this.latLon;
      console.log(this.itinerario);
    })

  
  }

}
