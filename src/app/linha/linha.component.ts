import { Component, OnInit } from '@angular/core';
import { LinhaOnibus } from '../shared/models/linhaOnibus.model';
import { LinhaOnibusService } from '../shared/service/linha-onibus.service';

@Component({
  selector: 'app-linha',
  templateUrl: './linha.component.html',
  styleUrls: ['./linha.component.css']
})
export class LinhaComponent implements OnInit {
  teste: any;
  linhasDeOnibus: LinhaOnibus[] = [];
  linhasDeOnibusNome: LinhaOnibus[] = [];
  selectedName: string | undefined;
  displayedColumns = ['id', 'codigo', 'nome'];


  constructor(private linhaService: LinhaOnibusService) { }

  ngOnInit(): void {
    this.linhaService.getLinhaOnibus().subscribe((linhasDeOnibus: LinhaOnibus[]) => {
      console.log(linhasDeOnibus);
      this.linhasDeOnibus = linhasDeOnibus;
    });
     console.log(this.linhasDeOnibus);
  }

  getPesquisaPorName(event: { value: string; }){
    let cont =0;
    this.selectedName = event.value;
    console.log(this.selectedName);
      for(let i =0 ; i<this.linhasDeOnibus.length ; i++){
          if(this.linhasDeOnibus[i].nome == this.selectedName){
            this.linhasDeOnibusNome[cont] = this.linhasDeOnibus[i];
            cont++;
          }
      }

  }

}
