import { Component, OnInit } from '@angular/core';
import { Lotacao } from '../shared/models/lotacao.model';
import { LotacaoService } from '../shared/service/lotacao.service';

@Component({
  selector: 'app-lotacao',
  templateUrl: './lotacao.component.html',
  styleUrls: ['./lotacao.component.css']
})
export class LotacaoComponent implements OnInit {
  lotacao: Lotacao[] = [];

  constructor(private lotacaoService: LotacaoService) { }

  ngOnInit(): void {
    this.lotacaoService.getLinhaOnibus().subscribe((lotacao: Lotacao[]) => {
      console.log(lotacao);
      this.lotacao = lotacao;
    } );

    console.log(this.lotacao);

  }

}
