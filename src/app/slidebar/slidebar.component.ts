import { Component, OnInit } from '@angular/core';
import { Route, Router } from '@angular/router';

@Component({
  selector: 'app-slidebar',
  templateUrl: './slidebar.component.html',
  styleUrls: ['./slidebar.component.css']
})
export class SlidebarComponent implements OnInit {
  routes: Route[] | undefined;

  constructor(  private router: Router) { }

  ngOnInit(): void {
   
    this.routes = this.router.config;
    console.log(this.routes);

    
  }

}
